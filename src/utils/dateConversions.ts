export function DateToString(d: Date) {
    return d.toLocaleDateString('fr', {day: 'numeric', month: 'numeric', year: 'numeric'});
}

export function StringToDate(s: string) {
    const day_array = s.split('/');
    return new Date(parseInt(day_array[2]), parseInt(day_array[1]) - 1, parseInt(day_array[0]));
}

export function DateToAdeString(d: Date) {
    // we need to go from 15/02/2021 to 2021-02-15
    let str = DateToString(d)
    let [day, month, year] = str.split('/')
    return `${year}-${month}-${day}`
}
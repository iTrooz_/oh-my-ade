export class Calendar{
    name: string;
    url: string;
    lastFetchContent: string | null = null;
    lastFetchDate: Date = new Date(0);

    constructor(name: string, url: string){
        this.name = name;
        this.url = url;
    }
}
// @ts-ignore
import ICAL from 'ical.js';
import * as converter from '../utils/dateConversions';
import { Calendar } from "./Calendar";

export class CourseModel {
    name: string;
    location: string;
    group: string;
    lastModification: string;
    start: Date;
    end: Date;
    uid: string;
    courseNumber;

    constructor(ev: Array<any>) {
        const stringStart = ev[1][1][3];
        const stringEnd = ev[1][2][3];
        this.name = ev[1][3][3];
        this.location = ev[1][4][3];
        if (this.location === '') {
            this.location = '(pas de salle définie)';
        }
        const description = ev[1][5][3].replaceAll('\n', ' ');
        const whereModification = description.lastIndexOf('(Modif');
        this.lastModification = description.substring(whereModification)
            .replace('(', '')
            .replace(')', '');

        if (whereModification !== -1) {
            this.group = description.substring(0, whereModification).trim();
        } else {
            const whereExportation = description.lastIndexOf('(Export');
            if (whereExportation !== -1) {
                this.group = description.substring(0, whereExportation);
            } else {
                this.group = description;
            }
        }
        this.start = new Date(stringStart);
        this.end = new Date(stringEnd);
        this.uid = ev[1][6][3];
        this.courseNumber = parseInt(this.name.substr(2, 3));
    }

    getDate() {
        return converter.DateToString(this.start);
    }

}

interface ResultWithStatus<T> {
    result: T,
    status: string,
    error: boolean;
}

async function fetchUrl(url: string): Promise<ResultWithStatus<string>> {
    return fetch('https://sautax.alwaysdata.net/p/api/proxy', {
        body: url,
        method: 'POST',
    })
        .then(value => value.text())
        .then((result) => {
            return { result, status: "", error: false };
        })
        .catch(() => {
            return { result: "", status: "error when fetching", error: true };
        });
}

async function makeRequest(bakedUrl: string) : Promise<ResultWithStatus<string>> {
    return new Promise((resolve, reject) => {
        const timer = setTimeout(() => {
            console.log("fetch timeout");
            // stop after the timeout
            resolve({
                result: "", status: "timeOut", error: true
            });
        }, 10000); // timeout after 10 seconds
        if (bakedUrl){ // remove the warning 
            fetchUrl(bakedUrl)
                .then((res) => {
                    clearTimeout(timer);
                    resolve(res);
                });
        }
    });
}

function bakeUrl(originalUrl: string) {
    let today = new Date(Date.now());
    let firstDate = new Date(today);
    firstDate.setDate(firstDate.getDate() - 30);
    let lastDate = new Date(today);
    lastDate.setDate(lastDate.getDate() + 70);
    let url = '';
    if (originalUrl.length > 0) {
        if (originalUrl.includes("firstDate")) {
            url = originalUrl.substring(0, originalUrl.lastIndexOf('&firstDate='));
            url += '&firstDate=' + converter.DateToAdeString(firstDate) + '&lastDate=' + converter.DateToAdeString(lastDate);
        } else {
            url = originalUrl;
        }
        return url;
    }
    return null;

}


function parse(content: string): ResultWithStatus<Map<string, Array<CourseModel>>> {
    let result = new Map() as Map<string, Array<CourseModel>>;
    try {

        let icalParsed = ICAL.parse(content);

        if (icalParsed && icalParsed.length && icalParsed.length > 2 && icalParsed[2]) {// check parsing

            // index by day
            for (const ev of icalParsed[2]) {
                let cours = new CourseModel(ev);
                let day = cours.getDate();
                let currentArray = result.get(day);

                if (!currentArray)
                    currentArray = [];
                currentArray.push(cours);
                result.set(day, currentArray);
            }

            // sort by hour
            result.forEach((day_components) => {

                if (day_components) {
                    day_components = day_components.sort((a, b) => {
                        return a.start.getTime() - b.start.getTime();
                    });
                }
            });
        } else  // parse error
            return { result, status: "parsingError", error: true };


    } catch (e) {
        return { result, status: "parsingError", error: true };

    }
    return { result, status: "", error: false };

}

export async function getCourses(calendar: Calendar, forceFetch = false): Promise<ResultWithStatus<Map<string, Array<CourseModel>>>> {

    let bakedUrl = bakeUrl(calendar.url);

    const today = new Date(Date.now());
    
    if (bakedUrl === null) {
        return { result: new Map(), status: "invalidUrl", error: true };
    }

    let madeRequest = false;
    if(forceFetch || !calendar.lastFetchContent || today.getTime()-calendar.lastFetchDate.getTime() >1000 * 60 * 60 * 12){ // if cache is more than 12h old
        madeRequest = true;

        let fetchRes = await makeRequest(bakedUrl);

        if (!fetchRes.error) {
            let parseRes = parse(fetchRes.result);
            if(!parseRes.error){
                calendar.lastFetchContent = fetchRes.result;
                calendar.lastFetchDate = new Date();
                return {result: parseRes.result, status: "fetch", error: false};
            }
        }
    }

    if(calendar.lastFetchContent){
        let parseRes = parse(calendar.lastFetchContent);
        if(!parseRes.error){
            if(madeRequest){
                return { result: parseRes.result, status: "fetchError+cache", error: false };
            }else{
                return { result: parseRes.result, status: "cache", error: false };
            }
        }
    }

    return {result: new Map(), status: "error", error: true};

}

export function OffsetDay(n: number, currentDay: string) {
    let d = converter.StringToDate(currentDay);
    d.setDate(d.getDate() + n);
    return converter.DateToString(d);
}
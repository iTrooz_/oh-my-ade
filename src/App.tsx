import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Component } from 'react';
import './App.css';
import CalendarRefresh, { compactEnum, modes, } from './components/CalendarRefresh';
import { Settings } from './components/Settings';
import { CourseModel, getCourses, OffsetDay } from "./model/CalendarModel";
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { ModeIcon } from './components/ModeIcon';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import SettingsSharpIcon from '@mui/icons-material/SettingsSharp';
import RefreshIcon from '@mui/icons-material/Refresh';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import LinearProgress from '@mui/material/LinearProgress';
import Snackbar from '@mui/material/Snackbar';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import { getStatusMessage } from './utils/statusMessage';
import * as converter from './utils/dateConversions';
import { FormControl, Select, MenuItem } from '@mui/material';
import { Calendar } from "./model/Calendar";

interface AppState {
    calendars: Array<Calendar>,
    calendarName: string,
    result: Map<string, Array<CourseModel>>,
    coursesStatus: string,
    loading: boolean,
    mode: modes,
    compact: compactEnum,
    showSnack: boolean;
    currentDay: string;
    settings: boolean;
}




export const theme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#3949ab',
        },
        secondary: {
            main: '#80cbc4',
        },
    },

});


function calculateSettings(mode: modes, day: string) {
    const rem = parseFloat(getComputedStyle(document.documentElement).fontSize);
    const screenWidthRem = window.innerWidth / rem;
    const isLarger = screenWidthRem > 130;
    const isLarge = screenWidthRem > 105;
    const isMedium = screenWidthRem > 60;

    if (mode === modes.auto) {
        mode = modes.one;
        if (isMedium)
            mode = modes.three;
        if (isLarge)
            mode = modes.week;
    }

    let compact = compactEnum.no;
    if (!isLarger && mode === modes.week)
        compact = compactEnum.yes;
    if (!isMedium && mode === modes.three)
        compact = compactEnum.yes;


    let nbDays: number;

    switch (mode) {
        case modes.three:
            nbDays = 3;
            break;
        case modes.week:
            nbDays = 7;

            //-- Set at the begining of the week

            let selectedDate = converter.StringToDate(day);
            const dayOfWeek = selectedDate.getDay();
            let diff = selectedDate.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1);
            day = converter.DateToString(new Date(selectedDate.setDate(diff)));
            break;

        case modes.one:
        default:
            nbDays = 1;
            break;
    }


    return { day, nbDays, mode, compact };

}

function getOldStorageCalendar(calendarName: string) : Calendar|null {
    let calendarUrl = localStorage.getItem('last_url');
    if(calendarUrl==null)return null;
     // if there is data from the old format
    let calendar = new Calendar(calendarName, calendarUrl);
    let lastFetchDate = localStorage.getItem('last_fetch_date');
    if(lastFetchDate){
        calendar.lastFetchDate = new Date(lastFetchDate);
        calendar.lastFetchContent = localStorage.getItem('last_fetch_content');
    }
    return calendar;
}

class App extends Component<any, AppState> {

    constructor(props: any) {
        super(props);

        let calendars : Array<Calendar>;
        let calendarsStr = localStorage.getItem('calendars');
        let calendarName = '';

        if(calendarsStr){
            calendars = JSON.parse(calendarsStr);
            calendars.forEach((calendar : Calendar) =>{
                calendar.lastFetchDate = new Date(calendar.lastFetchDate);
                return calendar;
            });
            
            if(calendars.length!==0){   
                calendarName = localStorage.getItem('last_name') || '';
                if(!calendars.find(element => element.name===calendarName)){
                    calendarName = calendars[0].name;
                }
            }

        }else{
            calendars = new Array<Calendar>();
            calendarName = "Sans nom";
            let calendar = getOldStorageCalendar(calendarName);
            if(calendar!==null)calendars.push(calendar);

            // On met à jour le stockage, et on supprime l'ancien format
            localStorage.clear();
            localStorage.setItem("calendars", JSON.stringify(calendars));
            localStorage.setItem("last_name", calendarName);
        }
        
        let settings = calendars.length===0;
        
        this.state = {
            calendarName,
            calendars,
            currentDay: converter.DateToString(new Date(Date.now())),
            showSnack: false,
            result: new Map<string, Array<CourseModel>>(),
            coursesStatus: "none",
            loading: false,
            mode: modes.auto,
            compact: compactEnum.auto,
            settings
        };
    }


    getCalendar(name: string = this.state.calendarName) : Calendar {
        let cal = this.state.calendars.find(el => el.name===name);
        if(cal)return cal;
        throw new Error("no calendar associated with name "+name);
    }

    async componentDidMount() {
        window.addEventListener('resize', () => {
            this.forceUpdate();
        });
        window.addEventListener('keydown', (ev) => this.handleKey(ev));

        if(!this.state.settings){
            await this.populate();
        }

    }

    componentWillUnmount() {
        window.removeEventListener('resize', () => {
            this.forceUpdate();
        });
        window.removeEventListener('keydown', (ev) => this.handleKey(ev));

    }


    handleKey(ev: KeyboardEvent) {

        const { nbDays } = calculateSettings(this.state.mode, this.state.currentDay);

        if (ev.key === "ArrowRight") {
            this.setState({ currentDay: OffsetDay(nbDays, this.state.currentDay) });
        }
        if (ev.key === "ArrowLeft") {
            this.setState({ currentDay: OffsetDay(-nbDays, this.state.currentDay) });
        }
    }


    async populate(forceRefresh = false) {

        this.setState({ loading: true });

        let calendar = this.getCalendar(this.state.calendarName);
        console.log("fetching url : " + calendar.url);

        let output = await getCourses(calendar, forceRefresh);
        // this.state.calendars potencially changed

        if(output.status==='fetch'){
            localStorage.setItem("calendars", JSON.stringify(this.state.calendars));
        }
        
        let result = output.result;
        
        if(output.error){
            console.log("An error occured while fetching");
        }else{
            console.log("using " + output.status + " ressource");
        }
        
        this.setState({ calendars: this.state.calendars, result, coursesStatus: output.status, loading: false, showSnack: true });
    }

    onValidateHandler(calendars : Array<Calendar>) {
        if(calendars.length===0)return;

        let calendarName = this.state.calendarName;
        if(!calendars.find(element => element.name===calendarName)){
            calendarName = calendars[0].name;
        }

        calendars = calendars.filter(element => element.name!=null && element.url!=null)

        localStorage.setItem("calendars", JSON.stringify(calendars));
        localStorage.setItem("last_name", calendarName);

        this.setState({ calendars, calendarName, loading: true, settings: false }, () => {
            this.populate(true);
        });

    }

    handleClose(
        _: any,
        reason?: string,
    ) {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ showSnack: false });
    };

    render() {
        const { day, nbDays, mode, compact } = calculateSettings(this.state.mode, this.state.currentDay);

        return (
            <ThemeProvider theme={theme}>

                <Box sx={{
                    backgroundColor: 'background.default',
                    minHeight: '100vh',
                    m: 0,
                    p: 0,
                }}>
                    <AppBar position="sticky">
                        <Toolbar color="primary">
                            {this.state.settings ?
                                <>
                                    {this.state.calendars.length >= 1 ?
                                    <>
                                        <IconButton edge="start" color="inherit" aria-label="retour" sx={{ mr: 2 }}
                                            onClick={() => { this.setState({ settings: false }); }}>
                                            <ArrowBackIcon />
                                        </IconButton>

                                        <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1 }}>
                                            Annuler
                                        </Typography>
                                    </>
                                    :
                                    <></>
                                    }
                                </>
                                :
                                <>
                                    <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1 }}>

                                        {
                                            mode === modes.one ?
                                                <>
                                                    {converter.StringToDate(day).toLocaleDateString('fr', {
                                                        day: 'numeric',
                                                        month: 'short',
                                                        weekday: 'short'
                                                    })}
                                                </>
                                                :
                                                <>
                                                    Oh my ade
                                                </>
                                        }
                                    </Typography>

                                    <FormControl>
                                    <Select
                                        key="slider-select-calendar"
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={this.state.calendarName}
                                        onChange={(event) => {
                                            this.setState({calendarName: event.target.value}, ()=>{
                                                this.populate()
                                            });
                                        }}
                                    >
                                        {this.state.calendars.map((option) => (
                                            <MenuItem key={option.name} value={option.name}>{option.name}</MenuItem>
                                        ))}

                                    </Select>
                                    </FormControl>

                                    <IconButton aria-label="jour précédent" onClick={() => {
                                        this.setState({ currentDay: OffsetDay(-nbDays, this.state.currentDay) });
                                    }}><NavigateBeforeIcon /></IconButton>
                                    <IconButton aria-label="Aujourd’hui" onClick={() => {
                                        this.setState({ currentDay: converter.DateToString(new Date(Date.now())) });
                                    }}>
                                        <CalendarTodayIcon />
                                    </IconButton>
                                    <IconButton aria-label="jour suivant" onClick={() => {
                                        this.setState({ currentDay: OffsetDay(nbDays, this.state.currentDay) });
                                    }}><NavigateNextIcon /></IconButton>
                                    <IconButton aria-label="changer l’affichage" onClick={() => {
                                        let mode = this.state.mode + 1;
                                        if (mode > modes.week)
                                            mode = modes.auto;

                                        this.setState({ mode });
                                    }}>
                                        <ModeIcon mode={this.state.mode} />
                                    </IconButton>
                                    <IconButton aria-label="actualiser" onClick={() => this.populate(true)}>
                                        <RefreshIcon />
                                    </IconButton>

                                    <IconButton edge="end" aria-label="paramètres" onClick={() => this.setState({ settings: true })}>
                                        <SettingsSharpIcon />
                                    </IconButton>
                                </>
                            }


                        </Toolbar>
                        {this.state.loading ?
                            <LinearProgress />
                            : <Box sx={{ height: "4px", width: "100%" }}></Box>}
                    </AppBar>
                    <div className="App">
                        <div className="App-content">
                            <div id={'content'}>
                                {
                                    this.state.settings ?

                                        (<Settings calendars={this.state.calendars}
                                            onValidate={(calendars) => this.onValidateHandler(calendars)} />)
                                        :
                                        (<> <CalendarRefresh day={day} error={this.state.coursesStatus==='error'}
                                            result={this.state.result}
                                            loading={this.state.loading} mode={mode}
                                            compact={compact}
                                            change={(direction) => {
                                                this.setState({ currentDay: OffsetDay((direction ? 1 : -1) * nbDays, this.state.currentDay) });
                                            }}
                                        />
                                            <Snackbar
                                                open={this.state.showSnack}
                                                autoHideDuration={6000}
                                                onClose={(event) => this.handleClose(event)}
                                                message={getStatusMessage(this.state.coursesStatus, this.getCalendar().lastFetchDate)}
                                            />
                                        </>)}

                            </div>

                            <Typography component="div" sx={{ mt: '2em' }}
                                className={'footer'}>Version {process.env.REACT_APP_VERSION} | <a
                                    href={'https://gitlab.com/nilsponsard/oh-my-ade'}>Code source</a></Typography>
                        </div>
                    </div>
                </Box>

            </ThemeProvider >


        );
    }
}


export default App;

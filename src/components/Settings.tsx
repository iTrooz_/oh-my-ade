import { TextField, Button, Box } from '@mui/material';
import { Component, ReactElement } from "react";
import { Calendar } from "../model/Calendar";
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';

interface SettingsProps {
    calendars: Array<Calendar>,
    onValidate: (calendars: Array<Calendar>) => void,
}

interface SettingsState{
    calendars: Array<Calendar>
}

export class Settings extends Component<SettingsProps, SettingsState> {

    constructor(props: any){
        super(props);
        let calendars = this.props.calendars;
        if(calendars.length===0) calendars.push(new Calendar("", ""));
        this.state = { calendars };
    }

    getCalendarForm(calendar : Calendar, k: number){
        return (
            <Box key={k} sx={{
                m: 1,
                width: { xs: '90%', sm: '90%', md: '75%', lg: '50%', xl: '40%' },

                paddingBottom: "20px",
                paddingTop: "20px",

                display: "flex",
                flexDirection: "row",
            }}>
                <Box key={k} sx={{

                    flex: 1,

                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}>
                    <TextField fullWidth
                        variant="standard"
                        label="Nom du calendrier"
                        error={calendar.name===''}
                        onChange={(event) => {
                            calendar.name = event.target.value
                            this.setState({calendars: this.state.calendars});
                        }}
                        defaultValue={calendar.name}
                        />

                    <TextField fullWidth
                        variant="standard"
                        label="URL du calendrier"
                        error={calendar.url===''}
                        onChange={(event) =>{
                            calendar.url = event.target.value;
                            this.setState({calendars: this.state.calendars});
                        }}
                        defaultValue={calendar.url}
                    />
                </Box>
                <IconButton onClick={()=>{
                    this.state.calendars.splice(k, 1);
                    this.setState({calendars: this.state.calendars});
                }}
                sx={{
                    marginLeft: "50px",
                }}>
                    <DeleteIcon />
                </IconButton>
            </Box>
        )
    }

    addCalendar(){
        let calendar = new Calendar("", "");
        this.setState({calendars: [...this.state.calendars, calendar]})
    }

    onValidate(){
        let filteredCalendars = this.state.calendars.filter(cal => cal.name !== "" && cal.url !== "");
        console.log(filteredCalendars);
        if(filteredCalendars.length>0){
            this.props.onValidate(filteredCalendars);
        }
    }

    render() {
        let calendarForms : ReactElement[] = [];
        for(let i=0;i<this.state.calendars.length;i++){
            calendarForms.push(this.getCalendarForm(this.state.calendars[i], i))
        }

        return (
            <Box id={'view'} sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
            }}>
                <h3> Calendriers </h3>
                <p className='url-label'>Url du calendrier en ical (<a
                    href={'https://gitlab.com/nilsponsard/oh-my-ade/-/wikis/Comment-obtenir-l%E2%80%99url-du-calendrier'}>comment
                    l'obtenir</a>) :
                </p>

                {calendarForms}
                
                <Button variant="contained"
                onClick={(event) => this.addCalendar()}
                >Ajouter un calendrier</Button>

                <Button variant="contained"
                onClick={(event) => this.onValidate()}
                >Sauvegarder</Button>
            </Box>
        );
    }
}
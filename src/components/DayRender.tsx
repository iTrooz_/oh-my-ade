import { Component } from 'react';
import Course from './CourseRefresh';
import * as converter from '../utils/dateConversions';
import { CourseModel } from "../model/CalendarModel";
import { getIndexNextCourse } from "../utils/nextCourse";
import { DateToString } from "../utils/dateConversions";
import './CalendarRefresh.css';
import { modes } from './CalendarRefresh';

interface CaledarProps {
    result: Map<string, Array<CourseModel>>,
    day: string;
    compact: boolean;
    column: number;
    mode: modes;
}


export default class CalendarRefresh extends Component<CaledarProps, unknown> {

    render() {
        const todayStr = DateToString(new Date(Date.now()));
        let res = this.props.result.get(this.props.day);
        let out = [];

        const dateStyle = {
            gridColumnStart: this.props.column
        };

        if (this.props.mode !== modes.one) {
            out.push(<div key="day" className={'day-date'} style={dateStyle}>
                {converter.StringToDate(this.props.day).toLocaleDateString('fr', {
                    day: 'numeric',
                    month: 'short',
                    weekday: 'short'
                })}
            </div>);
        }


        if (res && res.length > 0) {
            res = res.sort((a, b) => {
                return a.start.getTime() - b.start.getTime();
            });

            let currentCourseIndex = getIndexNextCourse(res);
            for (let index = 0; index < res.length; ++index) {

                let flatTop = false;
                let flatBot = false;

                if (index > 0 && res[index - 1].end.getTime() === res[index].start.getTime()) {
                    flatTop = true;
                }
                if (index + 1 < res.length && res[index + 1].start.getTime() === res[index].end.getTime()) {
                    flatBot = true;
                }

                out.push(<Course key={res[index].uid} input={res[index]}
                    current={index === currentCourseIndex && todayStr === this.props.day}
                    compact={this.props.compact} column={this.props.column} flatBot={flatBot}
                    flatTop={flatTop} />);
            }
        }

        return out;
    }
}
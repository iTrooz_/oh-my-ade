import {Application, Router} from 'https://deno.land/x/oak/mod.ts';

const app = new Application();
const router = new Router();
router.prefix('/p/api')
    .post('/proxy', (ctx) =>
        ctx.request.body({type: 'text'})
            .value
            .then(value => {
                return fetch(value)
            })
            .then(value => value.text())
            .then(result => {
                ctx.response.body = result;
                ctx.response.headers.append('Access-Control-Allow-Origin', '*');
            })
            .catch(reason => {
                ctx.response.status = 403
            })
    );

// send static content
app
    .use(router.routes());


app.addEventListener('listen', ({hostname, port}) => {
    console.log(
        `Start listening on http://${hostname}:${port}`,
    );
});

await app.listen({hostname: '::', port: 8100});